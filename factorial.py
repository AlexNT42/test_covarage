import sys


def fact(number):
    """
    Factorial function

    :arg n: Number
    :returns: factorial of n
    """
    if number == 0:
        return 1
    return number * fact(number - 1)


def div(number):
    """
    Just divide
    """
    res = 10 / number
    return res


def main(number):
    res = fact(number)
    print(res)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(int(sys.argv[1]))
