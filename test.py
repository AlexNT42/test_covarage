import unittest
from factorial import fact
from factorial import div

class TestFactorial(unittest.TestCase):
    """
    Our basic test class
    """

    def test_fact(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        res = fact(5)
        self.assertEqual(res, 120)

    def test_div(self):
        """
        test of division func
        """
        res = div(5)
        self.assertEqual(res, 2)


if __name__ == '__main__':
    unittest.main()